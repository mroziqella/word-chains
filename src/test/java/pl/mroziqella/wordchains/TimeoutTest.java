package pl.mroziqella.wordchains;

import org.junit.Before;
import org.junit.Test;

/**
 * Created by Mroziqella on 29.07.2017.
 */
public class TimeoutTest {
    private ImportDictionary importDictionary;
    private WordConverter wordConverter;

    @Before
    public void setUp(){
         importDictionary = new ImportDictionary(this.getClass()
                .getClassLoader()
                .getResource("wordlist.txt")
                .getPath());
         wordConverter = new WordConverter(importDictionary);
    }

    @Test(timeout = 1000)
    public void executionTimeTest(){
       wordConverter.changeWord("time", "fore");
    }

    @Test(timeout = 1000)
    public void executionTime2Test(){
       wordConverter.changeWord("ruby", "code");
    }
    @Test(timeout = 1000)
    public void executionTime3Test(){
        wordConverter.changeWord("fore", "time");
    }
}

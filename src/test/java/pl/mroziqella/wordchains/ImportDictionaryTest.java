package pl.mroziqella.wordchains;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.List;

/**
 * Created by Mroziqella on 29.07.2017.
 */
@RunWith(DataProviderRunner.class)
public class ImportDictionaryTest {

    private final String dictionaryPath = getClass()
            .getClassLoader()
            .getResource("wordlist_short.txt")
            .getPath();
    private ImportDictionary importDictionary;

    @Before
    public void setUp(){
        importDictionary= new ImportDictionary(dictionaryPath);
    }

    @DataProvider
    public static Object[][] dataImport() {
        return new Object[][] {
                { "dog", 2l},
                { "mayestict", 4l },
        };
    }


    @Test
    @UseDataProvider("dataImport")
    public void importTest(final String input, final long expected) throws IOException {
        List<String> load = importDictionary.load(input);

        Assert.assertEquals(expected,load.size());
    }

    @Test
    public void importLowerCaseTest() throws IOException {
        List<String> load = importDictionary.load("dog");

        Assert.assertEquals("dog",load.get(0));
    }

}

package pl.mroziqella.wordchains;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Mroziqella on 29.07.2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class WordConverterTest {

    @Mock
    private ImportDictionary importDictionary;
    private WordConverter wordConverter;

    @Before
    public void setUp() throws IOException {
        wordConverter = new WordConverter(importDictionary);

        List<String> dictionaryCat = Arrays.asList("cot", "cog", "dog");

        List<String> dictionaryDoge = Arrays.asList("dose", "oo4e", "dost", "rost", "rest");

        List<String> dictionaryNumber = Arrays.asList(
                "78900", "78345", "72345", "72345", "79255", "78987", "72345", "78345", "78905", "78945");

        List<String> dictionaryGold = Arrays.asList("geld", "goad", "load","lead");


        Mockito.when(importDictionary.load("cat")).thenReturn(dictionaryCat);
        Mockito.when(importDictionary.load("doge")).thenReturn(dictionaryDoge);
        Mockito.when(importDictionary.load("12345")).thenReturn(dictionaryNumber);
        Mockito.when(importDictionary.load("gold")).thenReturn(dictionaryGold);


    }

    @Test(expected = IllegalArgumentException.class)
    public void changeWordExceptionTest() throws Exception {
        wordConverter.changeWord("test", "test1");
    }

    @Test
    public void changeWordCatToDogTest() {
        List<String> expected = Arrays.asList("cat", "cot", "cog", "dog");

        Assert.assertEquals(expected, wordConverter.changeWord("cat", "dog"));
    }


    @Test
    public void changeWordDogeToRestTest() {
        List<String> expected = Arrays.asList("doge", "dose", "dost", "rost", "rest");

        Assert.assertEquals(expected, wordConverter.changeWord("doge", "rest"));
    }

    @Test
    public void changeWord12345To78900Test() {
        List<String> expected = Arrays.asList("12345", "72345", "78345", "78945", "78905", "78900"
        );

        Assert.assertEquals(expected, wordConverter.changeWord("12345", "78900"));
    }

    @Test
    public void changeWordGoldToLeadTest(){
        List<String> expected =  Arrays.asList(
                "gold", "goad", "load","lead");

        Assert.assertEquals(expected, wordConverter.changeWord("gold", "lead"));

    }
    @Test
    public void sameValuesTest(){
        List<String> expected =  Arrays.asList(
                "gold");

        Assert.assertEquals(expected, wordConverter.changeWord("gold", "gold"));
    }

    @Test(expected = NotFoundPathToWordExeption.class)
    public void notFoundRealWordsTest(){
        List<String> expected =  Arrays.asList(
                "gold");

        Assert.assertEquals(expected, wordConverter.changeWord("gold", "rrrr"));
    }

    @Test
    public void getChangedIndexTest() {
        Assert.assertEquals(0, wordConverter.getChangedIndex("dog", "cog", "cog"));
        Assert.assertEquals(-1, wordConverter.getChangedIndex("dog", "dog", "cot"));
        Assert.assertEquals(1, wordConverter.getChangedIndex("dog", "dag", "cat"));
        Assert.assertEquals(-1, wordConverter.getChangedIndex("dog", "dat", "cat"));
    }

}

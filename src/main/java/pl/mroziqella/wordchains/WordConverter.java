package pl.mroziqella.wordchains;

import java.io.IOException;
import java.util.*;

/**
 * Created by Mroziqella on 29.07.2017.
 */
class WordConverter {

    final ImportDictionary importDictionary;

    public WordConverter(ImportDictionary importDictionary) {
        this.importDictionary = importDictionary;
    }


    public List<String> changeWord(String actual, String expected) {
        if (actual.length() != expected.length()) {
            throw new IllegalArgumentException();
        }
        List<String> resultList = new ArrayList<>();
        Set<Integer> indexChanged = new HashSet<>();
        resultList.add(actual);
        List<String> dictionary = getDictionary(actual);

        checkWordForDictionary(actual, expected, dictionary, resultList, indexChanged);

        checkExpectedWordIsInDictionary(expected, resultList);

        if (resultList.size() == 2) {
            throw new NotFoundPathToWordExeption();
        }

        return resultList;

    }

    private void checkExpectedWordIsInDictionary(String expected, List<String> resultList) {
        if (!resultList.contains(expected)) {
            resultList.add(expected);
        }
    }

    int getChangedIndex(String oldString, String newString, String expect) {
        int index = changeLetter(oldString, newString);
        if (isDifferent(index)) {
            if (checkCorrectExchange(newString, expect, index)) {
                return index;
            }
        }
        return -1;
    }

    private void checkWordForDictionary(String actual, String expected, List<String> dictionary, List<String> resultList, Set<Integer> indexChanged) {
        if (actual.equals(expected)) {
            return;
        }
        for (String a : dictionary) {
            int index = getChangedIndex(actual, a, expected);
            if (index >= 0 && !indexChanged.contains(index)) {
                indexChanged.add(index);
                resultList.add(a);
                checkWordForDictionary(a, expected, dictionary, resultList, indexChanged);
                removeWrongPathInTree(index, expected, resultList, indexChanged);
            }
        }
    }

    private void removeWrongPathInTree(int index, String expected, List<String> resultList, Set<Integer> indexChanged) {
        if (!expected.equals(resultList.get(resultList.size() - 1))) {
            resultList.remove(resultList.size() - 1);
            indexChanged.remove(index);
        }
    }

    private List<String> getDictionary(String actual) {
        try {
            return importDictionary.load(actual);
        } catch (IOException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }


    private int changeLetter(String oldString, String newString) {
        int index = -1;
        for (int i = 0; i < oldString.length(); i++) {
            if (oldString.charAt(i) != newString.charAt(i)) {
                if (isDifferent(index)) {
                    return -1;
                }
                index = i;
            }
        }
        return index;
    }

    private Boolean isDifferent(int index) {
        return index >= 0 ? true : false;
    }

    private Boolean checkCorrectExchange(String newString, String expect, int index) {
        return newString.charAt(index) == expect.charAt(index);
    }
}

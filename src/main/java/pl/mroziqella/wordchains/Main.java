package pl.mroziqella.wordchains;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Mroziqella on 29.07.2017.
 */
public class Main {
    public static void main(String args[]){
        ImportDictionary importDictionary = new ImportDictionary(Main.class
                .getClassLoader()
                .getResource("wordlist.txt")
                .getPath());

        WordConverter wordConverter = new WordConverter(importDictionary);

        List<String> strings = wordConverter.changeWord(args[0],args[1]);

        System.out.println(Arrays.toString(strings.toArray()));


    }
}

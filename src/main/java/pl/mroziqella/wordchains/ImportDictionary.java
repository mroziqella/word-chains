package pl.mroziqella.wordchains;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mroziqella on 29.07.2017.
 */
class ImportDictionary {
    private final String dictionaryPath;

    public ImportDictionary(String dictionaryPath) {
        this.dictionaryPath = dictionaryPath;
    }

    public List<String> load(String actual) throws IOException {
        return readFile(actual);
    }

    private List<String> readFile(String actual) throws IOException {
        List<String> list = new ArrayList<>();
        File file = new File(dictionaryPath);
        BufferedReader br = new BufferedReader(new FileReader(file));
        String st;
        while ((st = br.readLine()) != null) {
            if(st.length()==actual.length()) {
                list.add(st.toLowerCase());
            }
        }
        return list;
    }

}
